TITLE Fibonacci EAX           (fib.asm)

; Program Description: Ch4 Programming Assignment
; Author: Terje Rene Nilsen
; Creation Date: 10/10/11
; Revisions: 
; Date: 10/13/11

INCLUDE Irvine32.inc
.data
	; insert variables here
	fibarray WORD 8 DUP(1)
.code
main PROC
		;  insert executable instructions here

	mov ecx, 5 ; loop counter
	mov eax, 0 ; null everything out
	mov ebx, 0 ; null everything out
	mov eax, 1 ;  start position
	mov ebx, 1 ;  start position
	mov esi, OFFSET fibarray

	;cheating
	mov [esi],0 ;first number is 0
	;add esi, 6 ; jumping over the next 2 places in array

	loop1:
	
		mov edx, eax ; save EAX in EDX before changing it
		add eax, ebx ; add EBX to EAX
		mov ebx, edx ; save what EAX used to be (via EDX) in EBX
		
	add esi, 2
	mov [esi], eax
		call DumpRegs
	
	LOOP loop1
	
	mov esi, OFFSET fibarray ; back to start
	mov ecx, 5 ; loop counter
	mov eax, 0 ; null out

	loop2: ; view array

		mov eax, [esi] ; mov to mem to view array, one by one
		call DumpRegs
		add esi, 2

	LOOP loop2

	exit

main ENDP

	; insert additional procedures here
END main