# Calculate the 5 first numbers of the Fibonacci sequence. #

Programming assignment for the Assembly programming course CSCI 3001 at Hawaii Pacific University, fall 2011.

It uses the Irvin32 library.

## Technology / topics ##
* Assembly
* Fibonacci
* Irvin32 library


 Program Description: Ch4 Programming Assignment

 Author: Terje Rene Nilsen

 Creation Date: 10/10/11

 Revisions: 

 Date: 10/13/11